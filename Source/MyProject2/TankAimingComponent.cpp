// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAimingComponent.h"
#include "TankBarrel.h"
#include "TankTurret.h"
#include "Projectile.h"



// Sets default values for this component's properties
UTankAimingComponent::UTankAimingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTankAimingComponent::BeginPlay()
{
	Super::BeginPlay();
	
	
}


void UTankAimingComponent::MoveBarrelTowards(FVector AimDirection)
{
	if (!ensure(Barrel) || !ensure(Turret))
		return;
	auto BarrelRotator = Barrel->GetForwardVector().Rotation();
	auto AimAsRotator = AimDirection.Rotation();
	auto DeltaRotator = AimAsRotator - BarrelRotator;
	Barrel->Elevate(DeltaRotator.Pitch);
	if (FMath::Abs(DeltaRotator.Yaw)<180)
	{
		Turret->Rotate(DeltaRotator.Yaw);
	}
	else
	{
		Turret->Rotate(-DeltaRotator.Yaw);
	}
	
}

void UTankAimingComponent::InitilizeAll(UTankBarrel* BarrelSet, UTankTurret* TurretSet)
{
	Barrel = BarrelSet;
	Turret = TurretSet;
}

int32 UTankAimingComponent::GetRoundsLeft() const
{
	return RoundsLeft;
}

// Called every frame
void UTankAimingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (RoundsLeft <= 0)
	{
		FiringState = EFiringState::OutOfAmmo;
	}
	else if ((GetWorld()->TimeSeconds - LastFireTime) < FireRate)
	{
		FiringState = EFiringState::Reloading;
	}
	else if (IsBarrelMoving())
	{
		FiringState = EFiringState::Aiming;
	}
	else
	{
		FiringState = EFiringState::Locked;
	}
}

void UTankAimingComponent::AimAt(FVector HitLocation)
{
	if (!ensure(Barrel))
		return;
	auto TankName = GetOwner()->GetName();
	FVector OutLaunchVel;
	BarrelStartLoc = Barrel->GetSocketLocation(FName("FirePoint"));
	bool HaveAimSolution = UGameplayStatics::SuggestProjectileVelocity(this, OutLaunchVel, BarrelStartLoc, HitLocation, LaunchSpeed,
		false,0,0,ESuggestProjVelocityTraceOption::DoNotTrace);
	
	if (HaveAimSolution)
	{
		AimDirection = OutLaunchVel.GetSafeNormal();
		MoveBarrelTowards(AimDirection);
	}

}



bool UTankAimingComponent::IsBarrelMoving()
{
	if (!ensure(Barrel))
		return false;
	BarrelForwardVector = Barrel->GetForwardVector();
	if (BarrelForwardVector.Equals(AimDirection,0.01))
	{
		return false;
	}
	return true;
}

EFiringState UTankAimingComponent::GetFiringState() const
{
	return FiringState;
}

void UTankAimingComponent::Fire()
{

	isReloaded = (GetWorld()->TimeSeconds - LastFireTime) > FireRate;

	if (!ensure(Barrel) || !ensure(ProjectileBP))
		return;
	if (FiringState == EFiringState::Locked || FiringState == EFiringState::Aiming)
	{
		FirePoint = Barrel->GetSocketTransform(FName("FirePoint"));
		auto ball = GetWorld()->SpawnActor<AProjectile>(ProjectileBP, FirePoint);
		ball->LaunchProjectile(LaunchSpeed);
		LastFireTime = GetWorld()->TimeSeconds;
		RoundsLeft--;
	}

}