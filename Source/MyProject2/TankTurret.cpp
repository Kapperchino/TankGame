// Fill out your copyright notice in the Description page of Project Settings.

#include "TankTurret.h"


void UTankTurret::Rotate(float RotationalSpeed)
{
	RotationalSpeed = FMath::Clamp<float>(RotationalSpeed, -1, 1);
	RotationChange = RotationalSpeed*MaxDegreesPerSec*GetWorld()->DeltaTimeSeconds;
	RawNewElevation = RelativeRotation.Yaw + RotationChange;
	SetRelativeRotation(FRotator(0,RawNewElevation,0));
}
