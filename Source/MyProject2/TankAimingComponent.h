// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "TankAimingComponent.generated.h"

UENUM()
enum class EFiringState : uint8
{
	Reloading,
	Aiming,
	Locked,
	OutOfAmmo,
};

class UTankBarrel;
class UTankTurret;
class ATank;
class UMovementComponent;
class AProjectile;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYPROJECT2_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTankAimingComponent();
	

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	void MoveBarrelTowards(FVector AimDirection);


	UFUNCTION(BlueprintCallable)
	void InitilizeAll(UTankBarrel* BarrelSet, UTankTurret* TurretSet);

	UPROPERTY(BlueprintReadOnly , Category = "State")
	EFiringState FiringState = EFiringState::Reloading;
	
	UFUNCTION(BlueprintCallable)
	int32 GetRoundsLeft() const;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void AimAt(FVector WorldSpace);

	bool IsBarrelMoving();
	
	EFiringState GetFiringState()const;
	UFUNCTION(BlueprintCallable)
		void Fire();
	UPROPERTY(EditDefaultsOnly)
		float LaunchSpeed = 6000;

private:
	UPROPERTY(EditDefaultsOnly, Category = Firing)
		float FireRate = 3;//how many per sec
	UPROPERTY(EditDefaultsOnly, Category = Setup)
		TSubclassOf<AProjectile> ProjectileBP;

	UTankBarrel* Barrel = nullptr;
	UTankTurret* Turret = nullptr;
	UGameplayStatics* GameSta = nullptr;
	bool isReloaded = false;
	FTransform FirePoint;
	float LastFireTime = 0;
	FVector AimDirection;
	FVector BarrelStartLoc;
	FVector BarrelForwardVector;
	 
	UPROPERTY(EditDefaultsOnly)
	int32 RoundsLeft = 9999;

	

	
	
	
};
