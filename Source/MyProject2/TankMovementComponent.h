// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/NavMovementComponent.h"
#include "TankMovementComponent.generated.h"

/**
 * 
 */
class UTankTrack;

UCLASS(meta = (BlueprintSpawnableComponent))
class MYPROJECT2_API UTankMovementComponent : public UNavMovementComponent
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
		void MoveForward(float amount);
	UFUNCTION(BlueprintCallable)
		void TurnRight(float amount);
	UFUNCTION(BlueprintCallable)
		void Initilize(UTankTrack* left, UTankTrack* right);

protected:
	UTankTrack* LeftTrack = nullptr;
	UTankTrack* RightTrack = nullptr;
	virtual void RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed) override;
	FVector AiDirection;
	FVector TankForwardVector;
	float CrossResult;
	float DotResult;
	 
	void BeginPlay() override;

	
	
};
