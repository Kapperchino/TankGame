// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/World.h"
#include "Components/StaticMeshComponent.h"
#include "TankTrack.generated.h"

/**
 * TankTrack is used to set driving force and to move the tank
 */
UCLASS(meta = (BlueprintSpawnableComponent))
class MYPROJECT2_API UTankTrack : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = Input)
		void SetThrottle(float Throttle);

protected:
	void BeginPlay() override;
	UPROPERTY(EditDefaultsOnly)
		float MaxForce = 400000;
	UPrimitiveComponent* TankRoot = nullptr;
	FVector ForceApplied;
	FVector ForceLocation;
	UTankTrack();
	float DeltaTime;
	float CurrentThrottle;
	
	void DriveTrack();
	void ApplySidewaysForce();

private:
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);
};
