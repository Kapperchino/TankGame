// Fill out your copyright notice in the Description page of Project Settings.

#include "TankMovementComponent.h"
#include "TankTrack.h"





void UTankMovementComponent::BeginPlay()
{
	
}
void UTankMovementComponent::MoveForward(float amount)
{
	if (!LeftTrack || !RightTrack)
		return;
	LeftTrack->SetThrottle(amount);
	RightTrack->SetThrottle(amount);
}

void UTankMovementComponent::TurnRight(float amount)
{
	if (!LeftTrack || !RightTrack)
		return;
	LeftTrack->SetThrottle(amount);
	RightTrack->SetThrottle(-amount);
}


void UTankMovementComponent::Initilize(UTankTrack* left, UTankTrack* right)
{
	
	LeftTrack = left;
	RightTrack = right;
}

void UTankMovementComponent::RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed)
{
	AiDirection=MoveVelocity.GetSafeNormal();
	TankForwardVector = GetOwner()->GetActorForwardVector();
	DotResult=TankForwardVector | AiDirection;
	CrossResult = FVector::CrossProduct(TankForwardVector,AiDirection ).Z;
	TurnRight(CrossResult);
	MoveForward(DotResult);
	
	
}


