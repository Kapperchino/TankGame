// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "TankAimingComponent.h"
#include "TankAi.generated.h"

class UTankAimingComponent;
class ATank;
/**
 * 
 */
UCLASS()
class MYPROJECT2_API ATankAi : public AAIController
{
	GENERATED_BODY()
public:
	void BeginPlay() override;

	
	
	virtual void Tick(float DeltaTime) override;

protected:

	APawn* tank = nullptr;
	APawn* PlayerTank = nullptr;
	UTankAimingComponent* AimingComponent = nullptr;
	UPROPERTY(EditDefaultsOnly)
	float AcceptedRadius = 3000;

	UFUNCTION()
		void OnPossedTankDeath();

	
private:

	virtual void SetPawn(APawn* InPawn) override;
	
	
};
