// Fill out your copyright notice in the Description page of Project Settings.

#include "TankController.h"
#include "Tank.h"
#include "TankAimingComponent.h"




void ATankController::BeginPlay()
{
	Super::BeginPlay();
	AimingComp = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	if(AimingComp)
	{
		FindAimingComponent(AimingComp);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ez"));
	}
	
}

void ATankController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AimTowardsCrosshair();
}

void ATankController::AimTowardsCrosshair()
{
	if (!ensure(AimingComp)||!GetPawn())
		return;
	FVector HitLocation;
	if (GetSightRayHitLocation(HitLocation))
	{
		AimingComp->AimAt(HitLocation);
	}
}

bool ATankController::GetSightRayHitLocation(FVector& OUTHitLocation) const
{
	int32 SizeX, SizeY;
	FVector LookDirection;
	FVector2D ScreenLocation;
	GetViewportSize(SizeX, SizeY);
	ScreenLocation = FVector2D(SizeX*CrossX, SizeY*CrossY);
	if (GetLookDirection(ScreenLocation,LookDirection))
	{
		return(GetHitLocation(LookDirection, OUTHitLocation));
	}

	return false;
}

bool ATankController::GetLookDirection(FVector2D ScreenLocation, FVector & LookDirection) const
{
	FVector CameraLocation;
	return(DeprojectScreenPositionToWorld(ScreenLocation.X, ScreenLocation.Y,
		CameraLocation, LookDirection));
}

bool ATankController::GetHitLocation(FVector LookDirection,FVector& HitTraceLocation) const
{
	FHitResult HitResult;
	FVector CameraLocation, EndLocation;
	CameraLocation=PlayerCameraManager->GetCameraLocation();
	EndLocation = CameraLocation + LookDirection*TraceRange;
	if (GetWorld()->LineTraceSingleByChannel(HitResult, CameraLocation,
		EndLocation, ECC_Visibility))
	{
		HitTraceLocation = HitResult.Location;
		return true;
	}
	HitTraceLocation = FVector(0);
	return false;
}

void ATankController::OnTankDeath()
{
	StartSpectatingOnly();
}

void ATankController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		auto PossesedTank = Cast<ATank>(InPawn);
		if (!ensure(PossesedTank))
			return;

		PossesedTank->OnDeath.AddUniqueDynamic(this, &ATankController::OnTankDeath);
	}
}

