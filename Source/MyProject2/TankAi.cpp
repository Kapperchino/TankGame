// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAi.h"
#include "TankAimingComponent.h"
#include "Tank.h"


void ATankAi::BeginPlay()
{
	Super::BeginPlay();
	
	
}

void ATankAi::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	tank = GetPawn();
	PlayerTank = GetWorld()->GetFirstPlayerController()->GetPawn();
	AimingComponent = tank->FindComponentByClass<UTankAimingComponent>();
	if (!ensure(PlayerTank&&tank&&AimingComponent))
		return;
	MoveToActor(PlayerTank, AcceptedRadius);
	AimingComponent->AimAt(PlayerTank->GetActorLocation());

	if (AimingComponent->GetFiringState() == EFiringState::Locked)
	{
		AimingComponent->Fire();
	}

}

void ATankAi::OnPossedTankDeath()
{
	if (!GetPawn())
		return;
	 GetPawn()->DetachFromControllerPendingDestroy();
}

void ATankAi::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		auto PossesedTank = Cast<ATank>(InPawn);
		if (!ensure(PossesedTank))
			return;

		PossesedTank->OnDeath.AddUniqueDynamic(this, &ATankAi::OnPossedTankDeath);
	}
}

