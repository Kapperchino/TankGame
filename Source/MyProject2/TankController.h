// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "TankController.generated.h"

class ATank;
class UTankAimingComponent;
/**
 * 
 */
UCLASS()
class MYPROJECT2_API ATankController : public APlayerController
{
	GENERATED_BODY()

protected:
	
	UPROPERTY(BlueprintReadOnly)
	ATank* Tank = nullptr;
	
	UTankAimingComponent* AimingComp = nullptr;
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	
	void AimTowardsCrosshair();

	

	bool GetSightRayHitLocation(FVector& OUTHitLocation) const;
	bool GetLookDirection(FVector2D ScreenLocation, FVector& LookDirection)const;

	bool GetHitLocation(FVector LookDirection, FVector& HitTraceLocation)const;
	

	UPROPERTY(EditDefaultsOnly)
		float CrossX = 0.5;

	UPROPERTY(EditDefaultsOnly)
		float CrossY = 0.5;

	UPROPERTY(EditDefaultsOnly)
		float TraceRange = 1000000.;
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Setup")
		void FindAimingComponent(UTankAimingComponent* AimingComponent);
	
	UFUNCTION()
		void OnTankDeath();
	
	virtual void SetPawn(APawn* InPawn) override;
	
};
