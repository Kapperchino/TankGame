// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/World.h"
#include "TankTurret.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class MYPROJECT2_API UTankTurret : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:
	virtual void Rotate(float RotationalSpeed);

private:
	UPROPERTY(EditDefaultsOnly, Category = Setup)
		float MaxDegreesPerSec = 23;
	
	float RotationChange;
	float RawNewElevation;
	
	
};
