// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/World.h"
#include "TankBarrel.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class MYPROJECT2_API UTankBarrel : public UStaticMeshComponent
{
	GENERATED_BODY()

protected:
	
public:
	virtual void Elevate(float RelativeSpeed);
	
private:
	UPROPERTY(EditDefaultsOnly, Category = Setup)
		float MaxDegreesPerSec = 20;
	UPROPERTY(EditDefaultsOnly, Category = Setup)
		float MaxElevation = 40;
	UPROPERTY(EditDefaultsOnly, Category = Setup)
		float MinElevation = 0;

	float ElevationChange;
	float RawNewElevation;
	
};
